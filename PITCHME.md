---?image=bcgForcepoint.png
## Installazione e gestione dei sistemi firewall Forcepoint / Stonesoft

9 - 11 Agosto 2017

</br>
CNR Sede Centrale - Aula Pareto

---?image=bcgForcepoint.png

### Day One
- Introduzione
- Nozioni di base di SMC
- Configurazione Firewall
- Gestione delle regole del Firewall
- Utilizzo del logging e del monitoring delle connessioni
+++?image=bcgForcepoint.png
### Day Two
- Uso del NAT
- Definizione delle VPN
- Oggetti avanzati
- Sezione “Overview” del SMC
- Gestione dei repport
- Brevi cenni sui sistemi in alta affidabilita
- Accenni alle regole autenticate
+++?image=bcgForcepoint.png
### Day Three
- Attivita’ di configurazione dei apparati per gli specifici casi d’uso dei partecipanti

<!---
Cominciamo con la carellata generale
--->

---?image=bcgForcepoint.png
### Introduzione
- Panoramica su Forcepoint
- Architettura Next Generation Firewall
- Componenti del NGFW:
    - Security Management Center (SMC)
    - Log Server
    - Engine in FW/VPN, IPS, L2FW
    - Web Portal Server

+++?image=bcgForcepoint.png
#### Panoramica su Forcepoint
![Image](assets/ForcepointPoints.jpg)

A cura del personale Forcepoint

+++?image=bcgForcepoint.png
#### Architettura Next Generation Firewall

A Next-Generation Firewall (NGFW) is an integrated network platform, combining a _traditional firewall_ with other network device filtering functionalities, such as an application firewall using in-line deep packet inspection (DPI), an intrusion prevention system (IPS), TLS/SSL encrypted traffic inspection, website filtering, QoS/bandwidth management, antivirus inspection, malware detection, and third-party identity management integration (i.e. LDAP, RADIUS, Active Directory).

Necessita' di nuovi approcci.

+++?image=bcgForcepoint.png
#### Architettura Next Generation Firewall
![Image](assets/core-competency-connectivity.gif)

+++?image=JeffHopper17306.jpg

### Componenti del NGFW |
- Security Management Center (SMC) |
- Log Server |
- Engine in FW/VPN, IPS, L2FW |
- Web Portal Server |
- Inoltre...
  - Sandbox | McAfee GTI | Endpoint Intelligence Agent | Sidewinder |
  - Open Platform Communications unified architecture (OPC UA) |
  - Forcepoint Web Security (ex Triton) |

+++?image=bcgForcepoint.png
### Security Management Center (SMC)
- Punto centrale della gestione dei _Engine_
- Ridurre complessita' sulla visione distribuita



---?image=bcgForcepoint.png

### Nozioni di base di SMC
- Introduzione all’intefaccia utente
- Come e dove reperire il client Stonesoft
- I domini in ambito Forcepoint
- Esercitazione pratica

+++?image=bcgForcepoint.png
#### Introduzione all'interfaccia utente
- SMC traccia le operazioni di tutti i componenti del Forcepoint NGFW.
- Permette di configurare ogni aspetto del firewall.
- Ofre diverse "vedute" su loro stato e configurazione,
- Costituisce il punto dove accedere ai log generati da _Engine_
- Funzione di _Auditing_ delle operazioni e storia di ogni elemento creato

+++?image=bcgForcepoint.png
#### Introduzione all'interfaccia utente
L'interfaccia divisa in vetrate:
![SMC-home](img/SMC-home.png)

+++?image=bcgForcepoint.png
#### Introduzione all'interfaccia utente
![SMC-overview](img/SMC-overview.png)


+++?image=bcgForcepoint.png
#### Stonesoft Management Client
Le immagini del client disponibili sul
https://cloud.cnr.it

Si puo registrare utenti aziendali sul sito del Forcepoint?


+++?image=bcgForcepoint.png
#### Stonesoft Management Client


Non usare opzione 2
```
===============================================================================
Select Operating Mode
---------------------
Select which cryptographic algorithms are used for encryption. For Common
Criteria certified installations, you must only use cryptographic algorithms
that are compliant with the Federal Information Processing Standard FIPS
140-2.
  ->1- Standard Cryptographic Algorithms
    2- Restricted Cryptographic Algorithms Compatible with FIPS 140-2
ENTER THE NUMBER FOR YOUR CHOICE, OR PRESS <ENTER> TO ACCEPT THE DEFAULT:: 2
===============================================================================
```
A breve passeremo a cifratura a 256 bit chiave SHA512withECDSA

+++?image=bcgForcepoint.png
#### I domini in SMC

- Shared Domain
- Remote Administrator Domains


---?image=bcgForcepoint.png
### Configurazione del firewall engine

Creazione **oggetto firewall** in SMC.
- definizione firewall |
- assegnare licenza |
- salvare configurazione iniziale |

Configurazione iniziale su **_Engine_**
- due commandi da ricordare: |
  - sg-reconfigure |
  - sg-contact-mgmt |

---?image=bcgForcepoint.png
### Gestione delle regole  FW
Oggetto _Policy_
Policy basate su Template.
![template](img/fwTemplate.png)

+++?image=bcgForcepoint.png
### Gestione delle regole FW
![policyFields](img/policyFields.png)

+++?image=bcgForcepoint.png
### Gestione delle regole FW
![policyActions](img/policyActions.png)

+++?image=bcgForcepoint.png




---?image=bcgForcepoint.png
```
docker run --rm=true -it --net=host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --name lukali  kali:lu3 /bin/bash
```
+++?image=bcgForcepoint.png
### Deep Inspection
![image1](SMB-inspectionIPSinfo.png)
pacchetto ispezionato da IPS fuori Linea

+++?image=bcgForcepoint.png
### Deep Inspection
![image2](img/SMB-inspectionTerminated.png)
pacchetto ispezionato dal firewall e bloccato da lui dopo

+++?image=bcgForcepoint.png
### Deep Inspection
![image3](img/SMB-notified.png)
Allert finale
